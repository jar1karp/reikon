﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThomsonReuters.Desktop.SDK.DataAccess.TimeSeries;

namespace reikon
{
    public class EikonTimeSerie
    {
        private Dictionary<DateTime, double> d;
        public bool isReady { get; set; }

        public EikonTimeSerie()
        {
            d = new Dictionary<DateTime, double>();
            isReady = false;
        }

        public void AddChunk(DataChunk chunk)
        {
            foreach (IBarData barRecord in chunk.Records.ToBarRecords())
            {                
                var close = barRecord.Close;
                var time = barRecord.Timestamp;
                if (close.HasValue && time.HasValue)
                d[time.Value.Date] = close.Value;
            }
            isReady = chunk.IsLast;
        }

        public double[] Values()
        {
            return d.Values.ToArray();
        }

        public DateTime[] Dates()
        {
            return d.Keys.ToArray();
        }
    }
}
